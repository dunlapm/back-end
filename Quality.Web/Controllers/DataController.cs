﻿using Quality.Web.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace Quality.Web.Controllers
{
    public class DataController : ApiController
    {
        public async Task<IHttpActionResult> Get(int pageNo = 1, int pageSize = 5)
        {
            var data = await SqlHelper.GetItems(pageNo, pageSize);
            return Json(data);
        }
    }
}
