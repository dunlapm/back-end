﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class QPLitem
    {
        public string PartNumber { get; set; }
        public string PartRevision { get; set; }
        public string PartName { get; set; }
        public string ToolDieSetNumber { get; set; }
        public bool QplFlag { get; set; }
        public bool OpenPo { get; set; }
        public string PartJurisdiction { get; set; }
        public string PartClassification { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public bool Ctq { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime? LastUpdateOn { get; set; }
    }
}