﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public enum QplSortColumn
    {
        PartNumber, Revision, PartName, ToolDieSetNumber, IsQualified, OpenPo, Jurisdiction,
        Classification, CompanyName, SupplierCodes, Ctq, UserName, LastUpdatedDateUtc
    }

    public enum SqlSortDirection
    {
        ASC,
        DESC
    }
}