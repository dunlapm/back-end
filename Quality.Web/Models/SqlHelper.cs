﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Quality.Web.Models
{
    public static class SqlHelper
    {
        //https://www.brentozar.com/archive/2014/01/dynamic-sorting/
        private static readonly string BaseQuery = @"SELECT
PartNumber, Revision, PartName, ToolDieSetNumber, IsQualified, OpenPo, Jurisdiction,
Classification, CompanyName, SupplierCodes, Ctq, UserName, LastUpdatedDateUtc

FROM
(SELECT 
ROW_NUMBER() OVER (ORDER BY PartNumber ASC) as rn,
p.PartNumber,
qpl.Revision,
p.PartName,
qpl.ToolDieSetNumber,
qpl.IsQualified,
qpl.OpenPo,
p.Jurisdiction,
p.Classification,
c.CompanyName,
cs.SupplierCodes,
qpl.Ctq,
u.UserName,
qpl.LastUpdatedDateUtc

FROM Parts p
INNER JOIN QualifiedPartsLists qpl on p.PartId = qpl.PartId
INNER JOIN Users u on qpl.LastUpdatedById = u.Id
INNER JOIN Companies c on qpl.SupplierCompanyId = c.Id
INNER JOIN CustomerSupplierXref cs on cs.CustomerCompanyId = qpl.CustomerCompanyId AND cs.SupplierCompanyId = qpl.SupplierCompanyId
) as ordered
WHERE rn BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize";

        public static async Task<IEnumerable<QPLitem>> GetItems(int pageNum = 1, int pageSize = 5)
        {
            var items = new List<QPLitem>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlData"].ConnectionString))
            {
                //var sortColumn = GetSortCol(sort, sortDir);
                using (var cmd = new SqlCommand(BaseQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@PageNum", pageNum);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);

                    await conn.OpenAsync();
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            items.Add(new QPLitem
                            {
                                PartNumber = reader["PartNumber"].ToString(),
                                PartRevision = reader["Revision"].ToString(),
                                PartName = reader["PartName"].ToString(),
                                ToolDieSetNumber = reader["ToolDieSetNumber"].ToString(),
                                QplFlag = Convert.ToBoolean(reader["IsQualified"]),
                                OpenPo = Convert.ToBoolean(reader["OpenPo"]),
                                PartJurisdiction = reader["Jurisdiction"].ToString(),
                                PartClassification = reader["Classification"].ToString(),
                                SupplierName = reader["CompanyName"].ToString(),
                                SupplierCode = reader["SupplierCodes"].ToString(),
                                Ctq = Convert.ToBoolean(reader["Ctq"]),
                                LastUpdateBy = reader["UserName"].ToString(),
                                LastUpdateOn = DateTime.Parse(reader["LastUpdatedDateUtc"].ToString())
                            });
                        }

                        return items;
                    }
                }
            }
        }

        private static string GetSortCol(string sort, string sortDir)
        {
            QplSortColumn sortColumn;
            if (!Enum.TryParse(sort, out sortColumn)) {
                sortColumn = QplSortColumn.PartNumber;
            }

            SqlSortDirection sortDirection;
            if (!Enum.TryParse(sortDir, out sortDirection))
            {
                sortDirection = SqlSortDirection.ASC;
            }

            return $"{sortColumn} {sortDirection.ToString().ToUpper()}";
        }
    }
}