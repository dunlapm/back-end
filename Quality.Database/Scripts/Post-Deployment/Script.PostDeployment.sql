﻿USE [$(DatabaseName)];

PRINT '****************Begin Post-Deployment script at ' +CONVERT(VARCHAR(30),GETDATE(),120) + '***********************'; 

:r .\Data.Companies.sql
:r .\Data.CustomerSupplierXref.sql
:r .\Data.Users.sql
:r .\Data.Parts.sql
:r .\Data.QualifiedPartsLists.sql

PRINT '****************End Post-Deployment script at ' +CONVERT(VARCHAR(30),GETDATE(),120) + '***********************';