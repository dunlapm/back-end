﻿
GO

PRINT N'Update default data in [dbo].[Parts]...';

GO

-- Parts
;MERGE INTO dbo.Parts AS Target
USING (
		select 1, 1, '2214A932-1', 'Beam', 'ITAR', 'U7' union all
		select 2, 1, '3425B321', 'Blade', 'ITAR', 'K0' union all
		select 3, 1, '342BD124-5', '', '', '' union all
		select 4, 1, '9888B212-3', 'Master Blade', 'ITAR', 'G9' union all
		select 5, 2, '45986021', '', 'ITAR', 'R4' union all
		select 6, 2, '3342D121-1', '', '', ''
) AS Source (PartId, CompanyId, PartNumber, PartName, Jurisdiction, Classification)
	ON  Target.PartId = Source.PartId
-- update matched rows
WHEN MATCHED THEN UPDATE 
	SET Target.CompanyId = Source.CompanyId,
		Target.PartNumber = Source.PartNumber,
		Target.PartName = Source.PartName,
		Target.Jurisdiction = Source.Jurisdiction,
		Target.Classification = Source.Classification
-- insert new rows
WHEN NOT MATCHED BY TARGET THEN	INSERT 
	VALUES (CompanyId, PartNumber, PartName, Jurisdiction, Classification);

GO