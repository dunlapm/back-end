﻿SELECT p.PartNumber,
qpl.Revision,
p.PartName,
qpl.ToolDieSetNumber,
qpl.IsQualified,
qpl.OpenPo,
p.Jurisdiction,
p.Classification,
c.CompanyName,
cs.SupplierCodes,
qpl.Ctq,
u.UserName,
qpl.LastUpdatedDateUtc

FROM Parts p
INNER JOIN QualifiedPartsLists qpl on p.PartId = qpl.PartId
INNER JOIN Users u on qpl.LastUpdatedById = u.Id
INNER JOIN Companies c on qpl.SupplierCompanyId = c.Id
INNER JOIN CustomerSupplierXref cs on cs.CustomerCompanyId = qpl.CustomerCompanyId AND cs.SupplierCompanyId = qpl.SupplierCompanyId
